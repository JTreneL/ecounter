import sys
#from selenium import webdriver
#import sqlite3

class cpustorage:
    def __init__(self,item,store,werehouse,price):
        self.item = item
        self.store = store
        self.werehouse = werehouse
        self.price = price
    
    def totalpricestorage(self):
        totalprice = self.store + self.werehouse *  self.price
        print(f"{totalprice} is total price of werehouse and store.")
    
    def storestatus(self):
        print(f"{self.store} in store.")

    def werehousetatus(self):
        print(f"{self.werehouse} in werehouse.")

    def getprice(self):
        print(f"{self.price} is price of {self.item}.")
    
    def getdiscount(self):
        price = self.price
        getdisc = self.price / 100 * 7 
        afterdisc = int(price) - int(getdisc)
        print(f"Optimal discount price for {self.item} is {afterdisc}.")
        
def main():
    pass

a = cpustorage("Intel",20,40,4000)
a.totalpricestorage()
a.storestatus()
a.werehousetatus()
a.getprice()
a.getdiscount()
if __name__=="__main__":
    sys.exit(main())
